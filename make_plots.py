import numpy as np
import gvar as gv
import sys
import pandas as pd
import os
import matplotlib.pyplot as plt

import matplotlib as mpl
mpl.rcParams['lines.linewidth'] = 1
mpl.rcParams['figure.figsize']  = [6.75, 6.75/1.618034333]
mpl.rcParams['font.size']  = 20
mpl.rcParams['legend.fontsize'] =  16
mpl.rcParams["lines.markersize"] = 5
mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['ytick.direction'] = 'in'
mpl.rcParams['xtick.labelsize'] = 12
mpl.rcParams['ytick.labelsize'] = 12
mpl.rcParams['text.usetex'] = True



def save_fig(fig, filename):

    project_path = os.path.normpath(os.path.join(os.path.realpath(__file__), os.pardir))
    if not os.path.exists(os.path.normpath(project_path+'/figs/')):
        os.makedirs(os.path.normpath(project_path+'/figs/'))

    filepath = project_path+'/figs/'+filename+'.pdf'
    fig.savefig(filepath, transparent=True, bbox_inches='tight')
    return fig

def plot_results(results):

    main_result_color = 'deepskyblue'
    colors = ['orchid','mediumaquamarine', 'salmon', 'darkorange', ] 
    y = 0
    y_color = -3
    labels = []

    # For convenience
    def blank_line():
        nonlocal y
        nonlocal labels
        nonlocal y_color
        y +=1
        y_color +=1
        y_color %= len(colors)
        labels.append('')


    def amend_plot(value, label, color, starred=False):
        nonlocal y
        nonlocal labels

        x = gv.mean(value)
        xerr = gv.sdev(value)
        plt.errorbar(x=x, y=y, xerr=xerr, yerr=0.0,
                    alpha=0.8, color=color, elinewidth=5.0)
        if starred:
            plt.plot(x, y, color=main_result_color, marker='*', mec=main_result_color, ms=10, zorder=3)
        labels.append(str(label))
        y += 1 
        

    # Initialize
    fig = plt.figure(figsize=(12, 9))
    plt.axes([0.145,0.145,0.85,0.85])

    # Show band
    avg = results['CalLat 20']
    pm = lambda g, k : gv.mean(g) + k*gv.sdev(g)
    plt.axvspan(pm(avg, -1), pm(avg, +1), alpha=0.3, color=main_result_color)

    # Show band
    avg = results['FLAG 20']
    pm = lambda g, k : gv.mean(g) + k*gv.sdev(g)
    plt.axvspan(pm(avg, -1), pm(avg, +1), alpha=0.8, color=colors[y_color])



    # First show other collaborations
    for collab in reversed(results):
        if collab not in ['CalLat 20', 'FLAG 20']:
            param_value = results[collab]
            amend_plot(param_value, collab, 'slategrey')

    plt.axhline(y-0.0, ls='--')
    blank_line()

    # CalLat result
    amend_plot(results['CalLat 20'], 'CalLat 20', main_result_color)
    

    # Format axes
    plt.xticks(fontsize=16)
    plt.yticks(range(len(labels)), labels)
    plt.tick_params(axis='y', left=False, labelsize=20)
    plt.ylim(-1, y)
    plt.xlabel('$F^\pm_K/F^\pm_\pi$', fontsize=24)
    plt.tight_layout()

    fig = plt.gcf()
    plt.close()

    return fig


if __name__ == "__main__":

    rms = lambda *arr : np.sqrt(np.sum([a**2 for a in arr]))

    results = {
        'FNAL/MILC 17' : gv.gvar(1.1950, rms(0.0015, 0.0018)),
        'ETM 14E' : gv.gvar(1.184, rms(0.012, 0.011)),
        'FNAL/MILC 14A' : gv.gvar(1.1956, rms(0.0010, 0.0026)),
        'ETM 13F' : gv.gvar(1.183, rms(0.014, 0.010)),
        'HPQCD 13A' : gv.gvar(1.1916, rms(0.0015, 0.0016)),
        'MILC 13A' : gv.gvar(1.1947, rms(0.0026, 0.0037)),
        'MILC 11' : gv.gvar(1.1872, rms(0.0042)),

        'FLAG 20' : gv.gvar(1.1932, 0.0019),

        'CalLat 20' : gv.gvar(1.1942, 0.0046)
    }

    save_fig(plot_results(results), 'collab_comparison')