\documentclass[usenames,dvipsnames]{beamer}
\title{Lattice calculation of $F_K/F_\pi$ from a mixed domain-wall on HISQ action}

\author[shortname]{
	\texorpdfstring{\textcolor{ProcessBlue}{Nolan Miller}}{}  \and 
	H.~Monge-Camacho \and
	C.~C.~Chang \and
	B.~H\"orz \and
	E.~Rinaldi \and
	D.~Howarth \and
	E.~Berkowitz \and
	D.~A~Brantley \and
	A.~S~Gambhir \and
	C.~K\"orber \and
	C.~J~Monahan \and
	M.~A.~Clark \and
	B.~Jo\'{o} \and
	T.~Kurth \and
	A.~Nicholson \and
	K.~Orginos \and
	P.~Vranas \and
	A.~Walker-Loud
}
					  
\date{October 30, 2020 }

\usepackage{braket}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage{slashed}

\usetheme{default}
\usecolortheme{dove}
\setbeamercolor{frametitle}{fg=RoyalPurple,bg=Orchid!20}

\addtobeamertemplate{navigation symbols}{}{%
	\usebeamerfont{footline}%
	\usebeamercolor[fg]{footline}%
	\hspace{1em}%
	\insertframenumber/\inserttotalframenumber
}
\setbeamertemplate{background}
{\includegraphics[height=\paperheight,keepaspectratio]{figs/background.jpg}}

\begin{document}

\frame{\titlepage}

\section{$F_K/F_\pi$}
\begin{frame}
	\frametitle{Why $F_K/F_\pi$?}
	\framesubtitle{The Cabibbo–Kobayashi–Maskawa Matrix}
	\begin{columns}
		\begin{column}{0.5\textwidth}
		{\color{ProcessBlue} Question:} Are the quark eigenstates of the weak and strong interaction the same?
		\begin{equation} \nonumber
		q_\text{weak} = V q_\text{strong}
		\end{equation}
		{\color{ProcessBlue} Answer:} No!
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=0.8\textwidth]{figs/kaon_box_diagram.png}
				\caption{(Wikipedia)}
			\end{figure}
		\end{column}
	\end{columns}
	\begin{equation}
	\begin{bmatrix}
	|V_{ud}| & |V_{us}| & |V_{ub}| \\
	|V_{cd}| & |V_{cs}| & |V_{cb}| \\
	|V_{td}| & |V_{ts}| & |V_{tb}|
	\end{bmatrix} = \begin{bmatrix}
	0.97446(10) & 0.22452(44) & 0.00365(12)\phantom{0} \\
	0.22438(44) & 0.97359(11) & 0.04214(76)\phantom{0} \\
	0.00896(24) & 0.04133(74) & 0.999105(32)
	\end{bmatrix} \nonumber
	\end{equation}
\end{frame}

\begin{frame}
	\frametitle{Unitarity of the CKM Matrix}
	Standard model predicts $V^\dagger V= 1$. In particular, we verify:
	\begin{equation} \notag
	\underbrace{|V_{ud}|^2}_{\substack{\text{Known from} \\ \text{experiments}}} +
	\underbrace{|V_{us}|^2}_{\substack{\text{Accessible} \\ \text{by lattice}}}  +
	\underbrace{|V_{ub}|^2}_{\substack{\text{Relatively} \\ \text{small}}} 
	= 1 
	\end{equation}
	Per Marciano:
	\begin{equation} \notag
	\frac{\Gamma(K \rightarrow l \, \overline{\nu}_l)}{\Gamma(\pi \rightarrow l \, \overline{\nu}_l)} =
	\left({\color{RubineRed}\frac{F_K}{F_\pi}} \right)^2 \frac{|V_{us}|^2}{|V_{ud}|^2} \frac{m_K (1 - m_l^2/ m_K^2)^2}{m_\pi (1 - m_l^2/ m_\pi^2)^2} \left[ 1 + \frac{\alpha}{\pi}(C_K - C_\pi) \right]
	\end{equation}
	\begin{equation*}
	\braket{0 | \overline{d} \gamma_\mu \gamma_5 u | \pi^+(p)}= i p_\mu {\color{RubineRed}F^+_\pi}
	\qquad
	\braket{0 | \overline{s} \gamma_\mu \gamma_5 u | K^+(p)} = i p_\mu {\color{RubineRed}F^+_K}
	\end{equation*}
\end{frame}

\begin{frame}
	\frametitle{Why \emph{Lattice} QCD?}
	$|V_{us}|$ ``easily'' accessed by lattice, not experiment
	\begin{itemize}
		\item probe $|V_{us}|$ via $K \rightarrow l \nu, \pi l \nu$
		\item kaons decay rapidly
	\end{itemize}
	\vfill
	Lattice QCD is a non-pertubative approach to QCD
	\begin{equation}
		\langle O \rangle = \frac{1}{Z_0} \int \mathcal{D}[q, \overline q] \mathcal{D}[A] \,  e^{iS_{\text{QCD}}[q, \overline q, A]} O[q, \overline q, A]\nonumber
	\end{equation}

	Lattice QCD pracitioners can:
	\begin{itemize}
		\item experiment with different discretizations of the QCD action
		\item control systematics (finite volume, lattice spacing, etc)
		\item fine-tune the QCD parameters to values different from those in our universe
		\item use lattice methods in tandem with EFT 
	\end{itemize}

\end{frame}

\begin{frame}
	\frametitle{Why $F_K/F_\pi$ via Lattice QCD?}
	$F_K / F_\pi$ is a \emph{gold-plated} quantity, serving as an important benchmark for testing lattice QCD actions
	\begin{itemize}
		\item dimensionless $\implies$ scale-setting unnecessary
		\item $F_K, F_\pi$ correlated $\implies$ high (sub-percent) precision
		\item mesonic, not baryonic $\implies$ no signal-to-noise problems
		\item full chiral expansion known to $O(m_\pi^4)$ (NNLO)
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Comparison of Lattice Actions}
	\begin{table}[]
		\begin{tabular}{l|lll}
			\begin{tabular}[c]{@{}l@{}}Fermion\\ action\end{tabular} & Doublers? & \begin{tabular}[c]{@{}l@{}}Good chiral\\ properties?\end{tabular} & \begin{tabular}[c]{@{}l@{}}Computational\\ cost?\end{tabular} \\ \hline
			Naive                                                    & Yes (16)  & Yes                                                               & Cheap                                                         \\
			Wilson-Clover                                            & No        & No                                                                & Cheap                                                         \\
			{\color{ProcessBlue} Staggered}                          & Yes (4)   & Yes                                                               & Cheap                                                         \\
			{\color{RubineRed} Domain-Wall}                          & No        & Yes                                                               & Expensive                                                     \\
			Overlap                                                  & No        & Yes                                                               & Expensive                                                    
		\end{tabular}
	\end{table}
	Our action:
	\begin{columns}
	\begin{column}{0.5\textwidth}
	\begin{itemize}
		\item {\color{ProcessBlue} sea quarks}: staggered
		\item {\color{RubineRed} valence quarks}: domain-wall
	\end{itemize}	
	\end{column}
	\begin{column}{0.5\textwidth}
	\begin{itemize}
		\item $N_f = 2 + 1 + 1$
		\item $\mathcal{O}(a^2)$ discretization errors
	\end{itemize}	
	\end{column}
\end{columns}
\end{frame}


\begin{frame}
	\frametitle{$F_K/F_\pi$ Models}
	Goal: Determine LECs $\implies$ extrapolate to physical point
	\begin{align*}
	\left(\frac{F_K}{F_\pi}\right)_\text{lattice} &= 1 
	+ {\color{JungleGreen} \delta \left(\frac{F_K}{F_\pi}\right)_\text{NLO}} 
	+ {\color{ProcessBlue} \delta\left(\frac{F_K}{F_\pi}\right)_\text{NNLO} }
	+ 	\delta\left(\frac{F_K}{F_\pi}\right)_\text{NNNLO} \\
	& \phantom{=}
	+ \delta \left(\frac{F_K}{F_\pi}\right)_\text{FV} 
	+ {\color{RubineRed} \delta \left(\frac{F_K}{F_\pi}\right)_\mu}
	+ {\color{RubineRed}  \delta \left(\frac{F_K}{F_\pi}\right)_{\Lambda_\chi}}
	+ {\color{Mulberry} \delta \left(\frac{F_K}{F_\pi}\right)_{\alpha_S}}
	\end{align*}

	\begin{columns}[T]
	\begin{column}{0.58\textwidth}
		Model choices:
		\begin{enumerate}
			\item {\color{JungleGreen} at NLO: ratio or taylor-expand}
			\item {\color{ProcessBlue} at NNLO: full $\chi$PT or counterterms only}
			\item {\color{RubineRed} $\mu^2 = \Lambda^2_\chi = 4 \pi \left\{F^2_\pi, F^2_K , F_\pi F_K\right\}$}
			\item {\color{Mulberry} include $\alpha_S$ term or not}
		\end{enumerate}
	\end{column}
	\begin{column}{0.42\textwidth}
		Input:
		\begin{itemize}
			\item $F_K$ and $F_\pi$
			\item $m_K$, $m_\pi$, and $m_\eta$
			\item mixed meson masses
			\item lattice spacing
		\end{itemize}
	\end{column}
\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Model Parameters}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{center}
				\begin{tabular}{rccc}
					order& $N_{L_i}$& $N_{\chi}$& $N_{a}$\\
					\hline
					{\color{Cerulean} NLO}     & 1     & 0         & 0\\
					{\color{YellowOrange} NNLO} & 7     & 2         & 2\\
					{\color{YellowGreen} NNNLO}& 0     & 3         & 3\\
					\hline
					Total   & 8     & 5         & 5
				\end{tabular}
			\end{center}
			Many LECs
			\begin{itemize}
				\item constrain with priors
				\item check priors using empirical Bayes
			\end{itemize}	
		\end{column}
		\begin{column}{0.6\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/FKFpi_vs_epi_convergence_xpt_nnnlo_FV_PP}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Model Averaging}
	Total number of fits: 
	\begin{equation*}
	\underbrace{2}_{\substack{\text{expanded} \\ \text{or ratio}}} \times 
	\underbrace{2}_{\substack{\text{full $\chi$pt} \\ \text{or ct-only}}}  \times 
	\underbrace{3}_{\substack{\text{cutoff} \\ \text{choice}}} \times 
	\underbrace{2}_{\substack{\text{incl. or} \\ \text{excl. $\alpha_S$}}} 
	= 24 
	\end{equation*}
	Weigh fits with Bayes factor
	\begin{itemize}
		\item marginalization $\implies$ can compare models with different parameters
		\item automatically penalizes overcomplicated models
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Comparison of Models}
	
	\begin{figure}
		\centering
		\begin{subfigure}[b]{.45\linewidth}
		\includegraphics[width=\linewidth]{figs/hist_FF}
		\setcounter{subfigure}{2}%
		\end{subfigure}

		\begin{subfigure}[b]{.45\linewidth}
			\includegraphics[width=\linewidth]{figs/hist_ct}
			\setcounter{subfigure}{0}%
			\end{subfigure}
			\begin{subfigure}[b]{.45\linewidth}
			\includegraphics[width=\linewidth]{figs/hist_ratio}
			\end{subfigure}
	\end{figure}
	\begin{equation*}
		\boxed{F^\pm_K/F^\pm_\pi = 1.1942(45)}
	\end{equation*}

\end{frame}



\begin{frame}
	\frametitle{Error Budget}
	\begin{columns}
		\begin{column}{0.36\textwidth}
			\begingroup \scriptsize
			\begin{align*}
			&F_K / F_\pi &= 1.1964 \pm 0.0044\\
			\cline{3-3}
			&\text{Statistical} & 0.0032 \\ 
			&\text{Disc} & 0.0020 \\
			&\text{Phys Point} & 0.0015 \\
			&\text{Model Unc} & 0.0015 \\
			&\text{Chiral} & 0.0012 \\
			&\text{Volume} & 0.0001  \\
			\cline{1-3}
			&\delta \left(\frac{F_K}{F_\pi} \right)_\text{SU(2)} &= -0.00215 \pm 0.00072
			\end{align*}
			\endgroup
		\end{column}
		
		\begin{column}{0.64\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/model_breakdown.pdf}
			\end{figure}
		\end{column}
\end{columns}
\begin{equation*}
\boxed{F^\pm_K/F^\pm_\pi = 1.1942(45)}
\end{equation*}
\end{frame}

\begin{frame}
	\frametitle{Previous Results}
	\begin{figure}
		\centering
		\includegraphics[width=1.0\textwidth]{figs/collab_comparison}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{$|V_{us}|$ from $F_K / F_\pi$}

	\begin{columns}
		\begin{column}{0.30\textwidth}
		This result:
		\begin{equation*}
			\frac{|V_{us}|}{|V_{ud}|} = 0.2311(10)
		\end{equation*}
		With experiment:
		\begin{equation*}
			|V_{us}| = 0.2251(10)
		\end{equation*}
	\end{column}


		\begin{column}{0.70\textwidth}
			
			\begin{figure}
				\centering
				\includegraphics[width=1.0\textwidth]{figs/VusVud.pdf}
			\end{figure}

			
		\end{column}

	\end{columns}
	\vfill
	\begin{equation*}
		\boxed{\sum_{q\in\{d, s, b\}} |V_{uq}|^2 = 0.99977(59)}
	\end{equation*}
\end{frame}

\begin{frame}
	\frametitle{Summary}
	In conclusion:
	\begin{itemize}
		\item $F_K/F_\pi \implies |V_{us}|$
		\item $F_K/F_\pi$ is a \emph{gold-plated} quantity and can be used to compare lattice QCD actions
		\item model averaging allows us to evaluate the fitness of many models without commiting to a single one
	\end{itemize}
\vfill
\begin{equation*}
\boxed{F^\pm_K/F^\pm_\pi = 1.1942(45)} \implies 
\boxed{\sum_{q\in\{d, s, b\}} |V_{uq}|^2 = 0.99977(59)}
\end{equation*}
\end{frame}





\section{Backup}

\begin{frame}
	\frametitle{Backup slides}
\end{frame}

\begin{frame}
	\frametitle{Model Averaging Procedure (1/2)}
	\begin{equation*}
		P(Y|D) = \sum_k P(Y | M_k, D) {\color{RubineRed} P(M_k | D)}
	\end{equation*}
	with model weights
	\begin{equation*}
		{\color{RubineRed}P(M_K | D)} = \frac{{\color{ProcessBlue} P(D | M_k)} P(M_k)}{\sum_l P(D | M_l) P(M_l)}
	\end{equation*}
	where ${\color{ProcessBlue} P(D | M_k)}$ is obtained by marginalization
	\begin{equation*}
		{\color{ProcessBlue} P(D | M_k)} = \int \prod_j \text{d} \theta_j^{(k)} \,  P(D | \theta_j^{(k)}, M_k) P(\theta_j^{(k)} | M_k)
	\end{equation*}
\end{frame}

\begin{frame}
	\frametitle{Model Averaging Procedure (2/2)}
	\begin{align*}
		\text{E}[Y] &= \sum_k \text{E}[Y | M_k] \, P(M_k | D)  \\
		\text{Var}[Y] &=
		\overbrace{
		\left[
		\sum_k \text{Var}[Y | M_k] P(M_k | D)
		\right]}^\text{model-averaged variance} \\
		&\phantom{=}
		+ \underbrace{\left[
		\left( \sum_k \text{E}^2[Y | M_k] \, P(M_k | D)\right)
		- \text{E}^2[Y | D]
		\right]}_{\left(\text{model uncertainty}\right)^2} \nonumber
		\end{align*}
\end{frame}

\begin{frame}
	\frametitle{Empirical Bayes Method}
	Let $M = \{\Pi, f\}$ denote a model. Per Bayes's theorem:
	\begin{equation*}
		{\color{RubineRed} p(\Pi | D, f)} 
		= \frac{{\color{ProcessBlue} p(D |\Pi, f)} {\color{JungleGreen} p(\Pi | f)}}{p(D | f)}
	\end{equation*}

	Assuming a uniform distribution for ${\color{JungleGreen} p(\Pi | f)}$:
	\begin{equation*}
		\text{peak of } {\color{ProcessBlue} p(D |\Pi, f)} 
		\implies \text{peak of }  {\color{RubineRed} p(\Pi | D, f)}
	\end{equation*}
	where ${\color{ProcessBlue} p(D |\Pi, f)} $ is the (readily available) Bayes factor
	\vfill
	\begin{itemize}
		\item Caveat: the uniformity assumption breaks down if we vary too many parameters separately or make our priors too narrow.  
	\end{itemize}
	
\end{frame}

\begin{frame}
	\frametitle{Isospin Correction}
	\begin{equation*}
		\delta_\text{SU(2)} =
		\frac{F_K^\pm}{F_\pi^\pm} - \frac{F_K}{F_\pi}
	\end{equation*}

	Per FLAG:
	\begin{align*}
		\delta_\text{SU(2)} \approx& 
		\sqrt{3}\epsilon_\text{SU(2)} \Bigg[ 
			-\frac43 \left( F_K/F_\pi - 1 \right) \\
			&+\frac{4}{3(4 \pi)^2 F_0^2} \left( m_K^2 - m_\pi^2 - m_\pi^2 \log \frac{m_K^2}{m_\pi^2}\right)
		\Bigg]
	\end{align*}
	Our paper:
	\begin{equation*}
		\delta_\text{SU(2)} \approx 
		\Big(F^\pm_K - F_\pi^\pm \Big)_\text{NLO} - \Big(F_K - F_\pi \Big)_\text{NLO}
	\end{equation*}
\end{frame}

\begin{frame}
	\frametitle{Example Model Corrections: NLO $\chi$PT w/ Taylor-Expanded Ratio}
	\begin{align*}
	\delta \left(\frac{F_K}{F_\pi}\right)_\text{NLO} 
	+\delta \left(\frac{F_K}{F_\pi}\right)_\text{FV} &=
	\frac{5}{8} \frac{\mathcal{I}(m_\pi)}{F^2}
	-\frac{1}{4} \frac{\mathcal{I}(m_K)}{F^2}
	-\frac{3}{8} \frac{\mathcal{I}(m_\eta)}{F^2} \\
	&\phantom{=}
	+ 4 \underbrace{\color{Plum} \frac{m_K^2 - m_\pi^2}{F^2}}_\text{SU(3) flavor} \underbrace{\color{RubineRed} L_5}_\text{LEC}
	\end{align*}
	where
	\begin{equation*}
	\mathcal{I}(m) = \frac{m^2}{(4\pi)^2} \ln \left( \frac{m^2}{\mu^2} \right)
	+ \frac{m^2}{4\pi^2} \underbrace{\color{ProcessBlue} \sum_{|\mathbf{n}|\neq0} \frac{c_n}{mL|\mathbf{n}|} K_1(mL|\mathbf{n}|) }_\text{finite volume}
	\end{equation*}
\end{frame}

\begin{frame}
	\frametitle{Software: \texttt{lsqfit}/\texttt{gvar}}
	\begin{columns}
		\begin{column}{0.6\textwidth}
			\begin{figure}
				\includegraphics[width=1.2\textwidth]{./figs/lsqfit_example.pdf}
			\end{figure}
		\end{column}
		\begin{column}{0.4\textwidth}
			\begin{itemize}
				\item Least-squares, Bayesian fitter (must specify priors)
				\item Tracks correlations between variables
				\item Allows errors-in-variables models
			\end{itemize}
		\end{column}
	\end{columns}
\end{frame}

\end{document}
