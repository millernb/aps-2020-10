# aps-2020-10

Notes & slides for APS Oct Meeting 2020

## Abstract
#### Lattice calculation of *F<sub>K</sub>/F<sub>π</sub>* from a mixed domain-wall on HISQ action.

I will present a lattice calculation of the ratio of the leptonic decay constants $`F_K/F_\pi`$. Per Marciano, $`F_K/F_\pi`$ can be related to the CKM matrix element $`|V_{us}|`$, therefore allowing us to probe the flavor-changing nature of the weak interaction and more generally providing constraints on the Standard Model. Further, $`F_K/F_\pi`$ serves as a gold-plated quantity in LQCD, providing a means to benchmark competing lattice techniques and actions. In our work, we employ a mixed-action with twisted-mass fermions in the sea and domain-wall fermions in the valence sector. We include five pion masses, ranging from 130$`~`$MeV to 350$`~`$MeV; four lattice spacings, ranging from 0.06$`~`$fm to 0.15$`~`$fm; and multiple lattice volumes. The extrapolation to the physical point is performed using SU(2) $`\chi`$PT to NNLO at 2-loops, from which we find $`F_{K^\pm}/F_{\pi^\pm}=1.1942(45)`$ with isospin breaking, consistent with the FLAG average value. Combining this result with experiment, we also determine $`|V_{us}|=0.2251(10)`$.
